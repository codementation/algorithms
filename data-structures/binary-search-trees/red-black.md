# Red Black Trees

## Rules

  * Every node is red or black
  * Root is always black
  * New inssertions are always red
  * Every pah from root -> leaf has the same number of black nodes
  * No path can have two consecutive red nodes
  * Nulls are black

## Rebalancing Rules

Rearrange the tree so that the median element becomes the root element

#### black uncle: **rotate**
  **Rules for rotation**

***When imbalance is in:***
* right child's right sub-tree
  * left rotation
* right child's left sub-tree
  * right-left rotation
* left child's right sub-tree
  * left-right rotation
* left child's left sub-tree
  * right rotation

***After rotation***

          BLACK
          /   \
        RED   RED

#### red uncle: **color flip**

  ***After color flip***

           RED
          /   \
       BLACK  BLACK


## Example

### Color Flip
```
    * == RED

      3                      *3       ------>     3
    /   \                    / \      color      / \
   *1   *5     color        1   5     flip      1   5
          \    flip              \                   \
          *7   ------>           *7                  *7

```

### Rotation
```
    * == RED

           3                     3                      3                    3
          / \                   / \                    / \                  / \
         1   5      rotate     1   5       rotate     1  *6     color      1   6
              \     right           \      left          / \    flip          / \
              *7    ------>         *6     ------>      5  *7   ------>     *5  *7
              /                       \                           
 insert ->  *6                        *7             

```
