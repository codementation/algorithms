// Using dynamic programming to solve the knapsack problem in Javascript
//
// The knapsack problem is a combinatorial optimization problem
//
// Given a set of items, each with a weight and a value,
// determine the items to include in a collection so that
// the total weight does not exceed a limit while maximizing
// the highest possible value of total items contained
// Items can be added repeatedly
//

// The existance of optimal substructure and overlapping subproblems
// means we can break down the problems into sub-problems
// and store their result so that we can reuse them.

// Top-down approach (Recursive)
// We use Hashmap to store the result of the already solved sub-problems
// using a key and then before solving the sub-problems
// first check if its solved then used that value, else solve it.
//
// Time complexity:  O(size(values) * capacity).
// Space complexity: O(size(values) * capacity).

// Constants
const values = [20, 5, 10, 40, 15, 25];
const weights = [1, 2, 3, 8, 7, 4];
const capacity = 10;
const map = new Map();

const knapSack = (values, weights, capacity, map, item) => {
    console.log('item: ' + item)
    console.log(map)

    // Base case: Negative capacity
    // MIN_SAFE_INTEGER insures recursion receives a negative value 
    // when including item results in a negative capacity
    if (capacity < 0) {
        console.log('cap is < 0: ' + capacity)
        //return Number.MIN_SAFE_INTEGER;
        return capacity
    }

    // Base case: No items left or no capacity
    if (capacity == 0 || item < 0) {
        return 0;
    }

    // Construct unique map key from the dynamic elements of the input
    const key = `${item}_${capacity}`;
    console.log('key is:' + key)
    console.log('value is:' + values[item])

    if (!map.has(key)) {
        // Case 1.  Include the current {item} in knapSack & recur for the
        // remaining items {item-1} with decreased capacity {target - weights[item]}
        let include =
            values[item] +
            knapSack(values, weights, capacity - weights[item], map, item - 1);
        console.log('include: ' + include)

        // Case 2.  Exclude the current {item} from knapSack & recur for the
        // remaining items {item-1}
        let exclude = knapSack(values, weights, capacity, map, item - 1);
        console.log('exclude' + exclude)

        // Compare and assign max value we get by including or excluding current item
        map.set(key, Math.max(include, exclude));
    }

    // Return solution to current sub-problem
    return map.get(key);
};

console.log(knapSack(values, weights, capacity, map, values.length - 1));

